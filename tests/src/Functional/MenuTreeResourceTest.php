<?php

namespace Drupal\Tests\rest_menu_tree\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Url;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\system\Entity\Menu;
use Drupal\Tests\rest\Functional\CookieResourceTestTrait;
use Drupal\Tests\rest\Functional\ResourceTestBase;

/**
 * Tests the menu tree resource.
 *
 * @group rest_menu_tree
 */
class MenuTreeResourceTest extends ResourceTestBase {

  use CookieResourceTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $auth = 'cookie';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'menu_link_content',
    'menu_normalizer',
    'rest_menu_tree'
  ];

  /**
   * {@inheritdoc}
   */
  protected static $resourceConfigId = 'menu_tree';


  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $auth = isset(static::$auth) ? [static::$auth] : [];
    $this->provisionResource([static::$format], $auth);

    // Create test menu for endpoint tests.
    $menu = Menu::create(['id' => 'test-menu', 'label' => 'Test Menu']);
    $menu->save();
    $menu_link1 = MenuLinkContent::create([
      'id' => 'test-link-1',
      'title' => 'Example.com',
      'link' => 'https://example.com',
      'weight' => 0,
      'menu_name' => 'test-menu',
    ]);
    $menu_link1->save();
    $menu_link2 = MenuLinkContent::create([
      'id' => 'test-link-2',
      'title' => 'Example.net',
      'link' => 'https://example.net/',
      'weight' => 1,
      'menu_name' => 'test-menu',
    ]);
    $menu_link2->save();
    $menu_link3 = MenuLinkContent::create([
      'id' => 'test-link-3',
      'title' => 'Example.org',
      'link' => 'https://example.org/',
      'weight' => 1,
      'menu_name' => 'test-menu',
    ]);
    $menu_link3->save();
  }

  /**
   * Tests GET request for the menu_tree resource.
   */
  public function testMenuTreeGet(): void {
    $this->initAuthentication();
    $url = Url::fromRoute('rest.menu_tree.GET', [
      'menu' => 'test-menu',
      '_format' => static::$format
    ]);
    $request_options = $this->getAuthenticationRequestOptions('GET');

    // Test user without required permission.
    $response = $this->request('GET', $url, $request_options);
    $this->assertResourceErrorResponse(
      403,
      "The 'restful get menu_tree' permission is required.",
      $response,
      ['4xx-response', 'http_response'],
      ['user.permissions']
    );

    // Test user with required permission.
    $this->setUpAuthorization('GET');
    $response = $this->request('GET', $url, $request_options);
    $this->assertResourceResponse(
      200,
      FALSE,
      $response,
      [
        'config:rest.resource.menu_tree',
        'config:system.menu.test-menu',
        'http_response',
        'menu_link_content:1',
        'menu_link_content:2',
        'menu_link_content:3',
        'menu_link_content_list'
      ],
      ['user.permissions'],
      FALSE,
      'MISS'
    );
    $menu_tree = Json::decode((string) $response->getBody());
    $this->assertCount(3, $menu_tree);
    $this->assertEquals('test-menu', $menu_tree[0]['link']['menu_name']);
    $this->assertEquals('test-menu', $menu_tree[1]['link']['menu_name']);
    $this->assertEquals('test-menu', $menu_tree[2]['link']['menu_name']);

    // Request an unknown menu.
    $url->setRouteParameter('menu', 'unknown');
    $response = $this->request('GET', $url, $request_options);
    $this->assertResourceErrorResponse(404, 'The "menu" parameter was not converted for the path "/entity/menu/{menu}/tree" (route name: "rest.menu_tree.GET")', $response);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUpAuthorization($method) {
    switch ($method) {
      case 'GET':
        $this->grantPermissionsToTestedRole(['restful get menu_tree']);
        break;

      default:
        throw new \UnexpectedValueException();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function assertNormalizationEdgeCases($method, Url $url, array $request_options): void {}

  /**
   * {@inheritdoc}
   */
  protected function getExpectedUnauthorizedAccessMessage($method) {
    // Not used for assertions in this test.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  protected function getExpectedUnauthorizedAccessCacheability() {
    // Not used for assertions in this test.
    return (new CacheableMetadata());
  }

}
